public class Makes10 {
    public static void main(String[] args) {
        int i = 9;
        int j = 9;
        boolean s = warmup1makes10(i, j);
        System.out.println(s);
    }
    private static boolean warmup1makes10(int a, int b) {
        int sum = a + b;
        return (sum == 10 || a == 10 || b == 10);
    }
}
